%global require_ibus_version 1.4.0

Name:		ibus-m17n
Version:	1.4.34
Release:	1
Summary:	The m17n engine for IBus
License:	GPL-2.0-or-later
URL:		https://github.com/ibus/ibus-m17n
Source0:	https://github.com/ibus/%{name}/releases/download/%{version}/%{name}-%{version}.tar.gz

BuildRequires: gnome-common gettext-devel m17n-lib-devel ibus-devel >= %{require_ibus_version}
BuildRequires:	libappstream-glib
BuildRequires:	gtk3-devel

Requires:   ibus >= %{require_ibus_version}
Requires:   m17n-lib

%description
ibus-m17n is an IME that uses input methods and corresponding icons in the m17n database.\
Unlike ibus-table which supports plain tables, m17n input methods also support states,\
whose labels are displayed on the IBus panel (language bar). M17n input methods also support\
surrounding text, consequently, languages such as Thai and IMs such as plain Zhuyin that\
require this feature are supported through ibus-m17n.

%prep
%autosetup -n %{name}-%{version} -p1
NOCONFIGURE=1 ./autogen.sh

%build
%configure --disable-static --with-gtk=3.0
%make_build

%install
%make_install
%find_lang %{name}

%check
appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/metainfo/*.appdata.xml
desktop-file-validate ${RPM_BUILD_ROOT}%{_datadir}/applications/ibus-setup-m17n.desktop
make check

%files -f %{name}.lang
%doc README
%license COPYING AUTHORS
%{_datadir}/metainfo/m17n.appdata.xml
%{_datadir}/ibus-m17n
%{_datadir}/icons/hicolor/16x16/apps/ibus-m17n.png
%{_datadir}/icons/hicolor/22x22/apps/ibus-m17n.png
%{_datadir}/icons/hicolor/24x24/apps/ibus-m17n.png
%{_datadir}/icons/hicolor/32x32/apps/ibus-m17n.png
%{_datadir}/icons/hicolor/48x48/apps/ibus-m17n.png
%{_datadir}/icons/hicolor/64x64/apps/ibus-m17n.png
%{_datadir}/icons/hicolor/128x128/apps/ibus-m17n.png
%{_datadir}/icons/hicolor/256x256/apps/ibus-m17n.png
%{_datadir}/icons/hicolor/scalable/apps/ibus-m17n.svg
%{_libexecdir}/ibus-engine-m17n
%{_libexecdir}/ibus-setup-m17n
%{_datadir}/ibus/component/*
%{_datadir}/applications/ibus-setup-m17n.desktop
%{_datadir}/glib-2.0/schemas/org.freedesktop.ibus.engine.m17n.gschema.xml

%changelog
* Wed Nov 27 2024 liyunqing <liyunqing@kylinos.cn> - 1.4.34-1
- Upgrade to version 1.4.34

* Wed Nov 29 2023 wangqia <wangqia@uniontech.com> - 1.4.24-1
- Upgrade to version 1.4.24

* Sat Sep 2 2023 liyanan <thistleslyn@163.com> - 1.4.22-1
- Upgrade to version 1.4.22

* Tue Dec 6 2022 liyanan <liyanan32@h-partners.com>  - 1.4.9-3
- fix strip problem

* Wed Nov 09 2022 xu_ping <xuping33@h-partners.com> - 1.4.9-2
- change source0 url

* Tue Jun 21 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.4.9-1
- Upgrade to version 1.4.9

* Mon Aug 02 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.4.1-3
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Fri Feb 14 2010 wangzhishun <wangzhishun1@huawei.com> - 1.4.1-2
- Package init
